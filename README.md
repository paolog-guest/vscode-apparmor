# vscode-apparmor

While we wait for an official Debian package for [Microsoft Visual Studio Code](https://code.visualstudio.com/) (see [ITP](https://bugs.debian.org/898259)), the only option is to use the vendor-supplied precompiled binaries (`.deb` file).

Here is an apparmor profile that prevents it from phoning back home.

Tested on debian 10 (buster) with the January 2019 release of vscode (version 1.31).

> This ATM does not work because of: https://bugs.debian.org/712451#101

YMMV; as a minimum you should add towards the end of `usr.share.code.code` the source code dirs that you want vscode to access.

Then to install, copy `usr.share.code.code` to `/etc/apparmor.d/usr.share.code.code` and turn it on:
```
aa-enforce /etc/apparmor.d/usr.share.code.code
```

To adapt it to your needs and/or keep it up to date, start **vscode** then in a root console: 
```
aa-logprof 
vi /etc/apparmor.d/usr.share.code.code 
apparmor_parser -vr /etc/apparmor.d/usr.share.code.code
```

To disable it:
```
aa-disable /etc/apparmor.d/usr.share.code.code
```

Merge requests are welcome if you find general issues.

## References:

- https://gitlab.com/apparmor/apparmor/wikis/Profiling_with_tools

- https://wiki.debian.org/AppArmor/HowToUse

- https://bugs.debian.org/712451
