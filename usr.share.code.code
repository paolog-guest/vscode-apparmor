#include <tunables/global>

/usr/share/code/code {
  #include <abstractions/X>
  #include <abstractions/base>
  #include <abstractions/fonts>
  #include <abstractions/user-tmp>

  # no network, no looking around
  deny network inet,
  deny network inet6,
  deny network raw,
  deny / r,

  # do not use system git (I will do that, thanks)
  deny /usr/bin/git x,

  # do not spawn bash
  deny /bin/bash x,

  # do not open files or URLs
  deny /usr/bin/xdg-open x,

  # execute ony these binaries but keep them in this apparmor profile
  /usr/bin/id mrix,
  /lib/x86_64-linux-gnu/ld-*.so ix,
  /usr/lib/go-*/bin/go mrix,
  /usr/share/code/*.so ix,
  /usr/share/code/code ix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/gc-signals/build/Release/gcsignals.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/native-keymap/build/Release/keymapping.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/native-watchdog/build/Release/watchdog.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/node-pty/build/Release/pty.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/oniguruma/build/Release/onig_scanner.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/spdlog/build/Release/spdlog.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/vscode-sqlite3/build/Release/sqlite.node mrix,
  /usr/share/code/resources/app/node_modules.asar.unpacked/vscode-ripgrep/bin/rg mrix,

  # only read from here
  /dev/shm/ r,
  /etc/gai.conf r,
  /etc/host.conf r,
  /etc/hosts r,
  /etc/init.d/ r,
  /etc/machine-id r,
  /etc/nsswitch.conf r,
  /etc/passwd r,
  /etc/pulse/client.conf r,
  /etc/pulse/client.conf.d/ r,
  /etc/pulse/client.conf.d/00-disable-autospawn.conf r,
  /etc/resolv.conf r,
  /proc/ r,
  /proc/sys/net/core/somaxconn r,
  /proc/vmstat r,
  /sys/bus/pci/devices/ r,
  /sys/devices/** r,
  /sys/kernel/mm/transparent_hugepage/enabled r,
  /usr/share/go-1.*/src/** r,
  /usr/share/bash-completion/bash_completion r,
  /usr/share/code/** r,
  /usr/share/icons/** r,
  /var/lib/dbus/machine-id r,
  owner @{HOME}/.config/Code/ r,
  owner @{HOME}/.profile r,
  owner @{HOME}/.vscode/** r,
  owner @{HOME}/.config/gtk-3.0/settings.ini r,
  owner @{HOME}/.config/user-dirs.dirs r,
  owner @{HOME}/.config/kioslaverc r,
  owner /proc/*/fd/ r,
  owner /proc/*/stat r,
  owner /proc/*/statm r,
  owner /proc/*/task/ r,
  owner /proc/*/task/** r,

  # you can write here
  /var/cache/fontconfig/ w,
  owner @{HOME}/.cache/fontconfig/ w,
  owner @{HOME}/.fontconfig/ w,
  owner /proc/*/oom_score_adj w,
  owner /run/user/** w,

  # can read/write here (k = can also lock)
  owner /dev/pts/* rw,
  owner /dev/shm/.org.chromium.Chromium.* rw,
  owner @{HOME}/.config/Code/** rwk,
  owner @{HOME}/.pki/nssdb/cert9.db rwk,
  owner @{HOME}/.pki/nssdb/key4.db rwk,
  owner @{HOME}/.pki/nssdb/pkcs11.txt rw,

  # list here the source code dirs that vscode may access
  owner /home/paolog/src/ratt/ r,
  owner /home/paolog/src/ratt/** rw,
}
